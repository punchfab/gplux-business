process.env.TZ = 'America/Bogota';


// const Buffer = require('buffer');
const express = require('express');
const puppeteer = require('puppeteer');
const readline = require('readline');
const bodyParser = require('body-parser');

var spinner = require("./functions/step");
const bank = require('./functions/bank');
// const prompt = require('prompt');

// Initializations

const app = express();
var publicDir = (__dirname+'/public/');
app.use(express.static(publicDir));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
/*const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});*/
// https://bancavirtual.bancoguayaquil.com/BMultiPersonas/indexAlternativoP.html
require('./src/database');
const sleep = (seconds) => new Promise(resolve => setTimeout(resolve, seconds * 1000));
const time = () => new Promise(resolve => resolve(((new Date().getTime())/1000).toFixed()));
const dateToday = (d = new Date()) => `${d.getDate() < 10 ? '0'+d.getDate() : d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;
// date >= 6 && date <= 11
// routes
const Sms = require('./models/sms');
const Transaction = require('./models/transactions');

app.get('/val/:entity/:code/', async (req, res) => {
	const {code, entity} = req.params;
	var sms = await Sms.find({ code:code, entity:entity });
	console.log(code);
	if (sms == 0) {
		const newSms = new Sms({ code, entity });
		await newSms.save();
		console.log(newSms);
	}

	// res.send('Verification');
	
	res.send('ok');
});

app.get('/transactions/', async (req, res) => {
	var dat = new Date();
	// var list = {"idTransaction":"19982606","process":"TRANSFERENCIA DIRECTA DE BARREZUETA  BULGARIN AROON ADRIAN","amount":1786,"type":"c","entity":"pichincha","date_entity":"5/12/2019"};
	let transaction = await Transaction.find({type: 'c'}).sort({ _id:-1 });

	/*let tr = await Transaction.find( list );

	let transaction = "false";
	if (tr.length > 0) transaction = "true";*/
	
	transaction.forEach(function(obj, index) {
		transaction[index].amount = Math.round((obj.amount / 100)*100)/100;
	});
	res.end(JSON.stringify({transaction}));
	// res.end(transaction);
});
app.get('/', async (req, res) => {
	// const trs = await Transaction.find({entity:"pichincha"});
	// let year = (new Date()).getFullYear();
	/*trs.forEach((transaction, index) => {
		trs[index].date_entity = dateToday(new Date(`${transaction.date_entity} ${year}`));
		trs[index].save();
	});*/
	// let n = trs.length;
	// console.log('number transactions',n);
	// console.info(trs);
	res.send(`number transactions`);
});
(async () => {
	// START
	// await sleep(100000);
	spinner.start("Iniciando Chrome\n");
	const startTimeAp = await time();
	const urls = {
		bg: 'https://bancavirtual.bancoguayaquil.com/BMultiPersonas/indexAlternativoP.html',
		bp: 'https://bancaweb.pichincha.com'
	};
	const browser = await puppeteer.launch({headless: false});
	// bg
	let bgActive = true;
	
	// console.log(endTime-startTime);
	spinner.stop("Iniciando Chrome ... listo!");
	let pageG;
	while (true) {

		if ((new Date()).getHours() >= 5 && (new Date()).getHours() <= 22) {
			try {
				// BG
				if (bgActive) {
					spinner.start("Abriendo BG\n");
					spinner.stop("Abriendo BG ... listo!\n");
					pageG = await browser.newPage();
					await bank.credentials (pageG, 'bg');
					let resultSecBg = await bank.securityCode(pageG, 'guayaquil');
					if (resultSecBg == 'success')
						bgActive = false;
					console.log(resultSecBg);
				}

				if (!bgActive) {
					// let dtNow = '14/11/2019';
					let dtNow = dateToday();
					let cond = true;
					let i = 0;
					let trListBg = [];
					let btnConsult = await pageG.evaluate(() => {
						let btn = document.querySelector('button[data-ng-click="btnConsultarClick()"]').getBoundingClientRect();
						return {
							x: btn.left + (btn.width / 2),
							y: btn.top + (btn.height / 2)
						};
					});

					await pageG.mouse.click(btnConsult.x, btnConsult.y);
					await pageG.waitFor(5000);
					// 2 opciones: la sesion sigue activa, la sesion caduco
					while(cond) {
						// while start
						let baseData = await pageG.evaluate((i) => {
							let listBg = document.querySelectorAll("tbody > tr[data-ng-model=\"easygrid.pagedItems\"]")[i];
							let btnDetail = listBg.querySelector('td:nth-child(12)>button').getBoundingClientRect();
							let idt = listBg.querySelector('td:nth-child(4)>span').innerText;
							return {
								idTransaction: idt == '' ? '0000' : idt,
								amount: (Number(listBg.querySelector('td:nth-child(7)').innerText.replace( /^\D+/g, '')) * 100).toFixed(),
								type: (listBg.querySelector('td:nth-child(11)>span').innerText == '( + )') ? 'c' : 'd',
								entity: 'guayaquil',
								date_entity: listBg.querySelector('td:nth-child(1)>span').innerText,
								x: btnDetail.left + (btnDetail.width / 2),
								y: btnDetail.top + (btnDetail.height / 2),
							};
						},i);
						if (baseData.date_entity != dtNow) {
							cond = false;
							break;
						}
						i++;

						await pageG.mouse.click(baseData.x, baseData.y);
						// div[data-ng-show="showCanalDetalle"]
						await pageG.waitForSelector('span[data-ng-bind="CodSecuencialCanal"]');
						await sleep(5);
						let detail = await pageG.evaluate(() => {
							let btnBack = document.querySelector('button[data-ng-click="btnRegresarClick()"]').getBoundingClientRect();
							return {
								proc: document.querySelector('span[data-ng-bind="CodSecuencialCanal"]').innerHTML.replace( /^\D+/g, ''),
								x: btnBack.left + (btnBack.width / 2),
								y: btnBack.top + (btnBack.height / 2),
							};
						});

						let {idTransaction, amount, type, entity, date_entity} = baseData;
						let { proc } = detail;
						// console.info(proc);
						let trBg = {idTransaction,  process: proc, amount, type, entity, date_entity};
						var tList = await Transaction.find(trBg);
						console.log("transaccion existente");
						console.info(tList);
						if (tList.length == 0) trListBg.unshift(trBg);

						await pageG.mouse.click(detail.x, detail.y);
						await sleep(5);
						// while end
					}
					// new entries
					if (trListBg.length > 0) {
						for (var c = 0; c < trListBg.length; c++) {
							let newTrBg = new Transaction(trListBg[c]);
							console.log(newTrBg);
							await newTrBg.save();
						}
						console.info('BG NUEVAS ENTRADAS');
					} else {
						console.info('BG NO HAY ENTRADAS');
					}

					// console.log(trListBg);
				}
			} catch (e) {
				bgActive = true;
				console.error("error producido ",e);
				await bank.closeTabs(browser, bgActive);
				// await browser.close();
				continue;
			}
			// END BG
			// Get the "viewport" of the page, as reported by the page.
			// x 538, y 275
			// BP
			try {
				const page = await browser.newPage();
				/*page.on('request', request => {
					const frame = request.frame();
					console.info('redireccionamiento bp');
					console.log(frame._navigationURL);
				});*/
				await bank.credentials(page, 'bp');
				// await page.reload();
				// await page.waitFor(5000);
				
				const sec = await page.evaluate(() => {
					if (document.getElementById('btn-login')) {
						if (document.querySelector('input[placeholder="Código de seguridad"]')) {
				        	return true;
						}
					}
					return false;
				});
				console.log('Security code:', sec);
				
				if (sec) {
					// Verification sms
					await bank.securityCode(page, 'pichincha');
				} else {
					await page.screenshot({path: './images/example.png'});
					const sms = await Sms.find({ entity: 'pichincha' });
					if (sms.length > 0)
						await Sms.findByIdAndDelete(sms[0].id);
				}

				await bank.getTransactions(page);
							
				console.log(Date(Date.now()).toString());
			
				let timeoutForm = false;
				timeoutForm = await page.evaluate(() => {
					let timeout = document.querySelector('form[name="sessionTimeoutForm"]');
					if (timeout) {
						let btnTimeout = timeout.querySelector('button').getBoundingClientRect();
						x = btnTimeout.left + (btnTimeout.width / 2);
						y = btnTimeout.top + (btnTimeout.height / 2);

						return { x: x, y: y};
					} else {
						return false;
					}
				});

				if (timeoutForm != false) {
					await page.mouse.click(timeoutForm.x, timeoutForm.y);
					console.log("Sesion expirara en 60 segundos");
					await page.screenshot({path: './images/example3.png'});
				} else {
					console.log("Sesion activa");
				}

			  	page.on('error', error => console.error(`❌ ${error}`));
			  	page.once('domcontentloaded', () => console.info('✅ DOM is ready'));
			  	await bank.closeTabs(browser, bgActive);
			  	// await sleep(60);
			} catch (e) {
				console.error("error producido ",e);
				await bank.closeTabs(browser, bgActive);
				// await browser.close();
				continue;
			}
		} else {
			console.log("Durmiendo");
			await sleep(3600);
		}
	  	const endTimeAp = await time();
	  	console.info(`${endTimeAp - startTimeAp} segundos transcurridos`);
	}
	await browser.close();
	// END


	/*do {
		await page.waitFor(30000);

	} while(!timeoutForm)*/

})();

app.listen(3000, () => {
	console.log('Server on port 3000');
});

// server.listen(8080);