const mongoose = require('mongoose');
const { Schema } = mongoose;

const TransactionSchema = new Schema ({
	idTransaction: { type: String, required: true },
	process: { type: String },
	amount: { type: Number, required: true },
	type: { type: String },
	entity: { type: String },
	date_entity: { type: String },
	date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Transaction', TransactionSchema);