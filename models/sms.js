const mongoose = require('mongoose');
const { Schema } = mongoose;

const SmsSchema = new Schema ({
	// phone: { type: String, required: true},
	// msg: { type: String},
	// bank: { type:String},
	status: { type: Boolean, default: true},
	code: { type: String, required: true}, 
	entity: { type: String },
	date: { type: Date, default: Date.now }
});

// mongoose.connect('mongodb://localhost:27017/node-login');
module.exports = mongoose.model('Sms', SmsSchema);