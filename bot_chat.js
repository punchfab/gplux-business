process.env.TZ = 'America/Bogota'


// const Buffer = require('buffer');
const express = require('express');
const puppeteer = require('puppeteer');
const readline = require('readline');
const bodyParser = require('body-parser');
const path = require('path');
const utils = require('./bot/utils');
const { createWorker } = require('tesseract.js');

const bank = require('./functions/bank.js');

const image = `/home/jekto/Imágenes/2.jpeg`;
const worker = createWorker();
(async ()=> {
  await worker.load();
  await worker.loadLanguage('spa');
  await worker.initialize('spa');
  const { data: { text } } = await worker.recognize(image);
  console.log(text);
})();

/*(async () => {
    let botjson = utils.externalInjection("/bot/bot.json");

	const browser = await puppeteer.launch({
		headless: false,
		userDataDir: path.join(process.cwd(), "ChromeSession")
	});
	page = await browser.newPage();
	await page.setViewport({
		width: 1280,
		height: 720,
		deviceScaleFactor: 1
	});
	page.setBypassCSP(true);
	await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36');
	await page.goto("https://web.whatsapp.com", {
		waitUntil: 'networkidle0',
		timeout: 0
	});
	botjson.then((data) => {
		page.evaluate("var intents = " + data);
        //console.log(data);
    }).catch((err) => {
        console.log("there was an error \n" + err);
    });
    page.exposeFunction("log", (message) => {
        console.log(message);
    })
    page.exposeFunction("getFile", utils.getFileInBase64);
	// await page.waitFor(30000);
	await page.screenshot({path: './images/whatsapp.png'});
	console.info('captura wp');

	var output = await page.evaluate("localStorage['last-wid']");
    //console.log("\n" + output);
    if (output) {
        console.info("Looks like you are already logged in");
        await injectScripts(page);
    } else {
        console.info("You are not logged in. Please scan the QR below");
    }

})();*/

async function injectScripts(page) {
    return await page.waitForSelector('[data-icon=search]')
        .then(async () => {
            var filepath = path.join(__dirname, "/bot/WAPI.js");
            await page.addScriptTag({ path: require.resolve(filepath) });
            filepath = path.join(__dirname, "/bot/inject.js");
            await page.addScriptTag({ path: require.resolve(filepath) });
            console.log("Inject start");
            return true;
        })
        .catch(() => {
            console.log("User is not logged in. Waited 30 seconds.");
            return false;
        })
}