const crypt = require('./security.js');
const config = require('../config/config.json');

const sleep = (seconds) => new Promise(resolve => setTimeout(resolve, seconds * 1000));
const time = () => new Promise(resolve => resolve(((new Date().getTime())/1000).toFixed()));
const dateToday = (d = new Date()) => `${d.getDate()}/${d.getMonth()+1}/${d.getFullYear()}`;

const Sms = require('../models/sms');
const Transaction = require('../models/transactions');

module.exports.credentials = async function (page, entity) {
	// https://bancavirtual.bancoguayaquil.com/BMultiPersonas/indexAlternativoP.html
	const url = {
		bg: 'https://bancavirtual.bancoguayaquil.com/BMultiPersonas/indexAlternativoP.html',
		bp: 'https://bancaweb.pichincha.com'
	};
	const pos = {
		bg: {
			x1: 590 , x2: 685,
			y1: 318, y2: 354, y3: 396
		},
		bp: {
			x1: 538, x2: 681,
			y1: 275, y2: 335, y3: 427
		}
	};
	const pass = {
		bg: await crypt.decryptText(config.b0.pass.bg, config.key),
		bp: await crypt.decryptText(config.b0.pass.bp, config.key)
	}

	await page.setViewport({
		width: 1280,
		height: 2900,
		deviceScaleFactor: 1
	});
	await page.goto(url[entity]);
	await page.waitFor(5000);
	await page.mouse.click(pos[entity].x1, pos[entity].y1);
	await page.keyboard.type(await crypt.decryptText(config.b0.user, config.key), {delay: 500}); // Types slower, like a user
	await page.mouse.click(pos[entity].x1, pos[entity].y2);
	await page.waitFor(5000);
	await page.keyboard.type(pass[entity], {delay: 500}); // Types slower, like a user
	await page.mouse.click(pos[entity].x2, pos[entity].y3);
	if (entity == 'bg') {
		await page.waitForSelector('input[data-ng-model="config.codigoSeguridad"]');
		await sleep(5);
	}
	else
		await page.waitForNavigation({ waitUntil: 'networkidle0', timeout: 60000 });
	console.info('Ingresando codigo de seguridad...');
	await page.screenshot({path: `${process.cwd()}/images/${entity}_login.png`});
	console.info('imgen capturada');
	// await page.waitFor(10000);
}

module.exports.closeTabs = async function (browser, bgActive)  {
	// true abrir nueva
	// false no abrir nueva ni cerrar
	let pages = await browser.pages();
	if (pages.length >= 2) {
  		console.log('tabs open', pages.length);
  		for (var i = 1; i < pages.length; i++) {
  			let url = await pages[i].url();
  			let result = url.search("bancavirtual.bancoguayaquil.com");
  			if (!bgActive && result != -1) {
  				console.log("bg activa");
  				continue;
  			} else if(bgActive)
  				console.info("bg terminada");
  			await pages[i].goto('about:blank');
  			await pages[i].close();
  		}
  	}
}

module.exports.securityCode = async function (page, entity) {
	const pos = {
		guayaquil: {x1:738, x2:690, y1:395, y2:465},
		//264
		pichincha: {x1:546, x2:672, y1:308, y2:378}
	};
	let timeoutSec = false;
	var sms = [];
	let dt = 0;
	const startTime = await time();
	do {
		sms = await Sms.find({ 
			status:true, 
			entity: entity, 
			date: {$gt: new Date((new Date()).getTime() - dt*1000)}
		}).sort({ _id:-1 }).limit(1);
		if (sms.length > 0) {
			await page.mouse.click(pos[entity].x1, pos[entity].y1);
			await sleep(5);
			await page.keyboard.type(sms[0].code.trim(), {delay: 500});
			let btnAcc = await page.evaluate(() => {
				let btn = document.querySelectorAll('button[data-ng-disabled="procesandoLogin"]')[7].getBoundingClientRect();
				return {
					x: btn.left + (btn.width / 2), 
					y: btn.top + (btn.height / 2)
				};
			});
			console.log(btnAcc);
			await page.mouse.click(btnAcc.x, btnAcc.y);
			console.log('code: ', sms[0].code);
			await Sms.findByIdAndDelete(sms[0].id);
			if (entity == 'guayaquil') {
				await page.waitForSelector('span[data-ng-bind="lbl.noRegistrar"]');

				await sleep(5);
				console.log('noRegistrar');
				let btnClick = await page.evaluate(() => {
					let btn = document.querySelector('span[data-ng-bind="lbl.noRegistrar"]').getBoundingClientRect();
					x = btn.left
					y = btn.top
					return {x: x, y: y};
				});

				console.log(btnClick);
				if (btnClick.x == 0 && btnClick.y == 0) {
					sms = [];
					continue;
				}

				await page.mouse.click(btnClick.x, btnClick.y);
				await page.waitForSelector('button[data-ng-click="easygrid.ConsultarEstadoCuenta(item)"]');
				/*.then(() => {
					console.error("CODIGO CORRECTO");
					return true;
				})
				.catch(() => {
					console.error("CODIGO INCORRECTO");
					return false;
				});;
				if (!nr) continue;*/
				// click account state
				await page.mouse.click(149, 288);
				await page.waitForSelector('.cabeceraGrid');
				await sleep(5);
				await page.screenshot({path: `${process.cwd()}/images/bgAccount.png`});
				console.log('bg completed');
			}
			else
				await page.waitForNavigation({ waitUntil: 'networkidle0' });
			// await page.screenshot({path: `${entity}2.png`});
		}
		let endTime = await time();
		dt = endTime - startTime;
		if (dt >= 180) {
			timeoutSec = true;
			console.info('tiempo excedido');
		}
	} while (sms.length == 0 && !timeoutSec)
	
	if (!timeoutSec) {
		// 149,288
		return 'success';
	}
	return 'failed';

}

module.exports.getTransactions = async function (page) {
	const year = (new Date()).getFullYear();
	const month = (new Date()).getMonth() + 1;
	// click account
	await page.mouse.click(543, 214);
	await page.waitForSelector('.transaction-date-divider.ng-binding', { timeout: 60000 });
	console.log('click account');
	// await page.waitFor(5000);
	await page.screenshot({path: `${process.cwd()}/images/example2.png`});
	// current quantity
	const countList = await page.evaluate(() => {
		return document.querySelectorAll(".transaction-list > .ng-scope:nth-child(1) > li").length;
	});
	// entity date
	let dtEnt = await page.evaluate(() => {
		return document.querySelector('.transaction-list > .ng-scope:first-child .transaction-date-divider.ng-binding').innerText.split(" ")[1];
	});
	// dateToday(new Date(`${dt} ${year}`))
	dtEnt = dateToday(new Date(`${month} ${dtEnt} ${year}`));

	console.log('lista ',countList);
	console.log('dt ',dtEnt);
	
	let trList = [];
	const trs = await Transaction.find({date_entity: dtEnt, entity: 'pichincha'});
	
	// new entries
	if (countList != trs.length) {
		let quantityNewEntries = countList - trs.length;
		for (let i = quantityNewEntries-1; i >= 0; i--) {
			let posContent = await page.evaluate(i => {
				// return i;
				var rect = document.querySelectorAll(".transaction-list > .ng-scope:nth-child(1) > li")[i].getBoundingClientRect();
				x = rect.left + (rect.width / 2);
				y = rect.top + (rect.height / 2);

				return {x: x, y: y};
			},i);

			// console.log(posContent);

			await page.mouse.click(posContent.x, posContent.y);
			// await page.waitForNavigation({ waitUntil: 'networkidle0' });
			// await page.waitFor(1000);
			await page.waitForSelector('.data-value.ng-binding', { timeout: 60000 });

			let list = await page.evaluate(( i, posContent, dateToday) => {
				var tr = document.querySelectorAll(".transaction-list > .ng-scope:first-child > li")[i];
				var year = (new Date()).getFullYear();
				var transactions = {
					idTransaction: (tr.querySelector(".data-value.ng-binding")) ? tr.querySelector(".data-value.ng-binding").innerHTML : "Nulo",
					process: tr.querySelector(".transaction-to.ty-data-label.ng-binding").innerHTML.trim(),
					amount: Number(tr.querySelector(".amount-whole-number").innerHTML + tr.querySelector(".amount-decimals").innerHTML),
					type: tr.querySelector(".amount-sign.ng-binding").innerHTML == '-' ? 'd' : 'c',
					entity: 'pichincha',
					/*date_entity: document.querySelector('.transaction-list > .ng-scope:first-child .transaction-date-divider.ng-binding').innerText,*/
					x: posContent.x,
					y: posContent.y,
				}

				return transactions;
			}, i, posContent, dateToday);
			list.date_entity = dtEnt;

			trList.push(list);
			const newTransaction = new Transaction(list);
			// newTransaction.
			console.log(newTransaction);
			// res.send('Verification');
			await newTransaction.save();
		}
		console.log(trList);
	} else {
		console.info("No hay nuevas entradas");
	}
}